require "auspost_parcel/version"

  module AuspostParcel

  # Auspost API class
  #
  # To be able to use the Austalian Post API, you must register and obtain an API key:
  #
  # => https://developers.auspost.com.au/apis/pacpcs-registration
  #
  # Create an API object using either AuspostStandard or AuspostExpress
  # 
  # call method:
  # => get_price_for(length, width, height, weight, from_postcode, to_postcode)
  # to obtain the result, which is a hash with the following keys:
  # :price                The calculated price. This is blank when an error occured.
  # :delivery_time        The expected delivery time. This is blank when an error occured.
  # :error                Error message. This is blank when the call was successful.
  #
  # The result should be the exact same as obtaibned when using the API tester on Australian Post:
  #
  # => https://developers.auspost.com.au/apis/pac/explorer/postage-parcel-domestic-calculate
  #
  # NB: It should be noted that the dimensions of the parcel (length, height, width) have little or no influence on the price.
  # The weight seems to be the only variable in the pricing.
  #

  class AuspostBase

    attr_reader :standard_price
    attr_reader :standard_delivery_time
    attr_reader :express_price
    attr_reader :express_delivery_time
    attr_reader :error_message

    TEMPLATE = 'curl -H "Accept: application/json" -H "Content-type: application/json" -H "AUTH-KEY: %s" -X GET -G -d "from_postcode=%s&to_postcode=%s&length=%.2f&width=%.2f&height=%.2f&weight=%.2f&service_code=%s" http://auspost.com.au/api/postage/parcel/domestic/calculate.json' 

    def initialize
      @error_message = nil
    end

    def error?
      @error_message.present?
    end

  protected

    def cube_root(x)
      x ** (1 / 3.0)
    end

    # def get_price_for_service(length, width, height, weight, service, pc_from, pc_to)
    def get_price_for_service(service, args)
      pc_from = args[:from_postcode]
      pc_to = args[:to_postcode]
      length = args[:length]
      width = args[:width]
      height = args[:height]
      weight = args[:weight]
      
      api_key = Rails.configuration.auspost_api_key rescue nil
      if api_key.nil?
        @error_message = "No Australian Post API key found in configuration (Add 'config.auspost_api_key = <your_key>' to config/#{Rails.env}.rb"
        return nil
      end
      cmd = TEMPLATE % [api_key, pc_from, pc_to, length, width, height, weight, service]
      res = JSON.parse `#{cmd}` rescue Hash.new
      @error_message = res['error']['errorMessage'] rescue nil
      res
    end
  end

  class AuspostStandard < AuspostBase

    def get_price_for(args = {})
      res = get_price_for_service("AUS_PARCEL_REGULAR", args)
      delivery_time = price = nil
      if @error_message.nil?
        delivery_time = res["postage_result"]["delivery_time"] rescue nil
        price = res["postage_result"]["total_cost"] rescue nil
      end
      { :price => price, :delivery_time => delivery_time, error: @error_message }
    end

  end

  class AuspostExpress < AuspostBase

    def get_price_for(args = {})
      res = get_price_for_service("AUS_PARCEL_EXPRESS", args)
      delivery_time = price = nil
      if @error_message.nil?
        delivery_time = res["postage_result"]["delivery_time"] rescue nil
        price = res["postage_result"]["total_cost"] rescue nil
      end
      { :price => price, :delivery_time => delivery_time, error: @error_message }
    end

  end

end
