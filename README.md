# AuspostParcel

A Gem for easy access to the Auspost API to get pricing information for parcels.

## Installation

Add this line to your application's Gemfile:

```ruby
gem "auspost_parcel", git: "https://bitbucket.org/sjorz/auspost_parcel.git"
```

And then execute:

    $ bundle install

## Configuration

Obtain an API key from Australia post through:

* https://developers.auspost.com.au/apis/pacpcs-registration

Put the API key in your configuration (application.rb, or development.rb and product.rb is you use different keys in dev and production):

```ruby
config.auspost_api_key = '<your_key>'
```

## Usage

Create an object for either Standard post or Express Post:

```ruby
ap_standard = AuspostParcel::AuspostStandard
ap_express = AuspostParcel::AuspostExpress
```

Then call:

```ruby
ap_standard.get_price_for(options)
ap_express.get_price_for(options)
```

where options is a hash containing length, width, height, weight, to_postcode and from_postcode, e.g:

```ruby
ap_standard.get_price_for(length: 10, width: 12, height: 6, weight: 0.4, to_postcode: 6000, from_postcode: 2000)
```

NB: Length, width, height in #centimeters#, weight in #kilograms#

This returns a hash with price, delivery time and an error message, e.g:

```ruby
{ :price => 7.45, :delivery_time => "Delivered in 5-6 business days", :error => nil }
```

If the call is successful, 'error' will be nil. If the call fails 'error' contains information on what went wrong and other fields are nil.

```ruby
'ap_standard.error?'
```

returns true if the call failed, else true.
