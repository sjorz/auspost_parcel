# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'auspost_parcel/version'

Gem::Specification.new do |spec|
  spec.name          = "auspost_parcel"
  spec.version       = AuspostParcel::VERSION
  spec.authors       = ["George Brautigam"]
  spec.email         = ["george@hyperactivedigital.com"]
  spec.summary       = %q{Simple Auspost Parcel Freight Charge Calculator.}
  spec.description   = %q{Calculate freight charges for parcels sent through Australia Post. Uses the API from Australia Post web site.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
